package utfpr.ct.dainf.if62c;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class ContadorPalavras {
    private BufferedReader reader;
    
    public ContadorPalavras(String caminho) throws IOException{
        reader = new BufferedReader(new FileReader(caminho));
    }
    
    public HashMap<String,Integer> getPalavras() throws IOException{
        HashMap<String,Integer> retorno = new HashMap<>();
        
        String linha;
        int numRepeat;
        
        while ((linha = reader.readLine()) != null) {
            for(String palavra:linha.split("[,.] *| +")){//[a-zA-Z0-9_.-]
                if(palavra != "" && palavra != null){
                    if(retorno.containsKey(palavra)){
                        numRepeat = retorno.get(palavra);
                        retorno.put(palavra, numRepeat + 1);
                    }else{
                        retorno.put(palavra, 1);
                    }
                }
            }   
        }
        
        reader.close();
        
        return retorno;
    }
}