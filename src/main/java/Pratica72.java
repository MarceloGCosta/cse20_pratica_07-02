import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.ContadorPalavras;
/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class Pratica72 {
    //COPIADO DO PEDRO
    public static void main(String[] args) {
        String caminho;
        ContadorPalavras contador;
        Scanner teclado = new Scanner(System.in);
        HashMap<String,Integer> ocorrencias;
        
        System.out.println("Digite o caminho completo de seu arquivo:");
        caminho = teclado.next();
        
        try{
            contador = new ContadorPalavras(caminho);
            ocorrencias = contador.getPalavras();
            
            List<Map.Entry<String,Integer>> listaEntradas = new ArrayList<>(ocorrencias.entrySet());
            
            Collections.sort(listaEntradas, (o1,o2)-> -o1.getValue().compareTo(o2.getValue()));
            
            caminho += ".out";
            
            BufferedWriter writer = new BufferedWriter(new FileWriter(caminho));
            
            for(Map.Entry<String, Integer> palavra: listaEntradas) {
                writer.write(palavra.getKey()+','+palavra.getValue());
                writer.newLine();
            }
            writer.close();
        }catch(IOException ioe){
            System.out.println(ioe.getMessage());
        }
    }
}
